## Interface: 90200
## Title: Domination Socket Helper
## Author: Metriss - Stormrage
## Version: v2.0.10
## SavedVariables: DSHDB
## Notes: Helps with changing Gems and Domination Socket Shards

## X-Curse-Project-ID: 507074

#@no-lib-strip@
libs\libs.xml
#@end-no-lib-strip@

#locales
locales\enUS.lua
locales\zhCN.lua
locales\zhTW.lua

main.lua
ldb.lua
options.lua

## Title: |cff880303[爱不易]|r Domination 统御碎片助手
## X-Vendor: AbyUI
