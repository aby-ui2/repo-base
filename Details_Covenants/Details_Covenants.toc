## Interface: 90200
## Title: Details!|Skada Covenants
## Notes: Show covenant icon
## Notes-ruRU: Показывает значок ковенанта в Details! или Skada
## Author: Toenak
## Version: 1.3.3
## X-License: MIT
## X-Curse-Project-ID: 431558
## SavedVariablesPerCharacter: DCovenant, DCovenantLog

Utils.lua
SpellIDs.lua
Oribos.lua

Details.lua
Skada.lua
Core.lua
Commands.lua

## Title-zhCN: |cff880303[爱不易]|r 伤害统计盟约图标
## X-Vendor: AbyUI
