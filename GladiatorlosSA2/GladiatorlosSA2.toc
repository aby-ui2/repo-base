## Interface: 90200
## Title: GladiatorlosSA
## Notes: PvP Sound Alerter
## Author: xGrit
## SavedVariables: GladiatorlosSADB
## Version: 3.13

#embeds.xml
Locales\locales.xml

GladiatorlosSA2.lua
spelllist.lua
options.lua

#ClassicEra\options_TBC.lua
#ClassicEra\spelllist_TBC.lua

## Title: |cff880303[爱不易]|r Gladiator PVP语音提示
## X-Vendor: AbyUI
